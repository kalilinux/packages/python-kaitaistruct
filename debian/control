Source: python-kaitaistruct
Section: python
Priority: optional
Maintainer: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 9), dh-python, python-all, python-setuptools, python3-all, python3-setuptools
Standards-Version: 3.9.8
Homepage: http://kaitai.io
X-Python-Version: >= 2.7
X-Python3-Version: >= 3.3
Vcs-Git: git://git.kali.org/packages/python-kaitaistruct.git
Vcs-Browser: http://git.kali.org/gitweb/?p=packages/python-kaitaistruct.git

Package: python-kaitaistruct
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}
Description: Kaitai Struct API for Python (Python 2)
 This package contains a library that implements Kaitai Struct API for Python.
 Kaitai Struct is a declarative language used for describe various binary data
 structures, laid out in files or in memory: i.e. binary file formats, network
 stream packet formats, etc.
 .
 It is similar to Python’s [construct] and [Construct3], but it is
 language-agnostic. The format description is done in YAML-based .ksy format,
 which then can be compiled into a wide range of target languages.
 .
 This package installs the library for Python 2.

Package: python3-kaitaistruct
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-python-kaitaistruct-doc
Description: Kaitai Struct API for Python (Python 3)
 This package contains a library that implements Kaitai Struct API for Python.
 Kaitai Struct is a declarative language used for describe various binary data
 structures, laid out in files or in memory: i.e. binary file formats, network
 stream packet formats, etc.
 .
 It is similar to Python’s [construct] and [Construct3], but it is
 language-agnostic. The format description is done in YAML-based .ksy format,
 which then can be compiled into a wide range of target languages.
 .
 This package installs the library for Python 3.
